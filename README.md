Pour installer le framwork CSSwag, rien de plus simple : Il suffit de cloner le repository et de tout récuperer.
Si vous souhaitez la version css, récuperez simplement le csswag.css dans le dossier css.
Si vous souhaitez la version scss, vous pouvez récuperer ce qu'il y a dans le dossier scss (Cependant, les mixins de grilles se situent dans le _grid.scss)